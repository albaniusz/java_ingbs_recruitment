package com.codility.tasks.spring.healthcheck;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
class HealthcheckController {
	@GetMapping(value = "/healthcheck", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, String> healthcheck(@RequestParam String format) {
		Map<String, String> response = new HashMap<>();

		switch (format) {
			case "short":
				response.put("status", "ok");
				break;
			case "full":
				response.put("currentTime", ZonedDateTime.now().toString());
				response.put("application", "OK");
				break;
			default:

		}

		return response;
	}

	@PutMapping(value = "/healthcheck")
	public void healthcheckPut() {
		return;
	}

	@PostMapping(value = "/healthcheck")
	public void healthcheckPost() {
		return;
	}

	@DeleteMapping(value = "/healthcheck")
	public void healthcheckDelete() {
		return;
	}
}
