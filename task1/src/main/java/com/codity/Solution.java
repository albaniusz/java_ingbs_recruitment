package com.codity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Solution {

	public static void main(String[] args) throws ParseException {
		Solution solution = new Solution();
		System.out.println(solution.solution("Mon", 2));
	}

	public String solution(String S, int K) throws ParseException {
		SimpleDateFormat dayFormat = new SimpleDateFormat("E", Locale.US);
		String response = "";

		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dayFormat.parse(S));
			calendar.add(Calendar.DAY_OF_MONTH, K);

			response = dayFormat.format(calendar.getTime());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return response;
	}
}
