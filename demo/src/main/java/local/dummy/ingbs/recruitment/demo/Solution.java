package local.dummy.ingbs.recruitment.demo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This is a demo task.
 * <p>
 * Write a function:
 * class Solution { public int solution(int[] A); }
 * that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
 * For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
 * Given A = [1, 2, 3], the function should return 4.
 * Given A = [−1, −3], the function should return 1.
 * Write an efficient algorithm for the following assumptions:
 * N is an integer within the range [1..100,000];
 * each element of array A is an integer within the range [−1,000,000..1,000,000].
 */
public class Solution {
	public static void main(String[] args) {
		Solution solution = new Solution();

		print(solution.solution(new int[]{1, 2, 3}));
		print(solution.solution(new int[]{-1, -3}));
		print(solution.solution(new int[]{1, 3, 6, 4, 1, 2}));
	}

	public static void print(int number) {
		System.out.println(number);
	}

	public int solution(int[] A) {
		List<Integer> list = Arrays.stream(A)
				.boxed()
				.sorted()
				.collect(Collectors.toList());

		int result = -1;
		int lastOne = 0;
		for (int i : list) {
			if (i > lastOne + 1) {
				result = lastOne + 1;
				break;
			}
			lastOne = i;
		}

		if (result < 0) {
			result = lastOne > 0 ? lastOne + 1 : 1;
		}

		return result;
	}
}
