package com.codility;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class VersionTest {
	// ... write your unit tests here ...

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Test
	public void shouldCheckEmptyParameter() {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage(errorVersionMustNotBeNull);

		new Version("");
	}

	@Test
	public void shouldCheckWrongVersionNamePattern() {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage(errorVersionMustMatchPattern);

		Version version = new Version("FOO");
		version.isSnapshot();
	}

	@Test
	public void shouldNotCompareVersionWithNull() {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage(errorOtherMustNotBeNull);

		Version versionA = new Version("1.1.1-SNAPSHOT");

		versionA.compareTo(null);
	}

	@Test
	public void shouldCompareVersionEquals() {
		Version versionA = new Version("1.1.1-SNAPSHOT");
		Version versionB = new Version("1.1.1-SNAPSHOT");

		assertEquals(0, versionA.compareTo(versionB));
	}

	@Test
	public void shouldCompareVersionLower() {
		Version versionA = new Version("1.1.1-SNAPSHOT");
		Version versionB = new Version("1.1.0-SNAPSHOT");

		assertEquals(1, versionA.compareTo(versionB));
	}

	@Test
	public void shouldCompareVersionHigher() {
		Version versionA = new Version("1.1.0-SNAPSHOT");
		Version versionB = new Version("1.1.1-SNAPSHOT");

		assertEquals(-1, versionA.compareTo(versionB));
	}

	@Test
	public void exampleTest() {
		Version version = new Version("3.8.0");
		// ...
	}

	@Test
	public void exampleTest2() {
		Version version = new Version("3.8.0-SNAPSHOT");
		// ...
	}

	// expected error messages:

	static final String errorVersionMustNotBeNull = "'version' must not be null!";
	static final String errorOtherMustNotBeNull = "'other' must not be null!";
	static final String errorVersionMustMatchPattern = "'version' must match: 'major.minor.patch(-SNAPSHOT)'!";
}
