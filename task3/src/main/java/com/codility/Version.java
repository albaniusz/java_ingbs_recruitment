package com.codility;

public class Version implements Comparable<Version> {
	private String version;

	public Version(String version) {
		if (version.isEmpty()) {
			throw new IllegalArgumentException("'version' must not be null!");
		}
		this.version = version;
	}

	boolean isSnapshot() {
		if (!version.matches("\\d+(\\.\\d+){0,2}(-SNAPSHOT)?")) {
			throw new IllegalArgumentException("'version' must match: 'major.minor.patch(-SNAPSHOT)'!");
		}
		return true;
	}

	@Override
	public int compareTo(Version o) {
		if (o == null) {
			throw new IllegalArgumentException("'other' must not be null!");
		}

		int versionNum = Integer.parseInt(version.replaceAll("\\D", ""));
		int versionOtherNum = Integer.parseInt(o.getVersion().replaceAll("\\D", ""));

		return versionNum - versionOtherNum;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
